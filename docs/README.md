# Introducción
Bienvenido a UIkit, UIkit es una libreria que te ayuda a diseñar interfaces para tus proyectos hechos con PyQt5 facilitandote el trabajo
ya que con menos código haces más cosas y asi te concentras en la logica de tu app.

Esta guía toma 30 minutos y pretende mostrarte los conceptos basicos que necesitas para desarrollar tus interfaces, mientras sigues esta guia
estaras desarrollando una aplicacion con muy poco código.

## Requisitos
Antes de comenzar debes tener instalado ```UIkit``` en tu proyecto y contar con una version de Python superior a la 3.5 y el gestor de paquetes ```pip```

## Parametros globales
Los parametros globales pueden ser utilizados en todas las clases y son los siguentes:
- pos
- stylesheet
- font

## Botones
Para iniciar un boton solo tienes que instanciar la clase ```Button``` y el contenedor padre donde será renderizado. Esta clase recibe los siguientes parametros opcionales:
- text
- pos
- btn
- icon
- stylesheet
- font (en desarrollo)

### Text
Este parametro es opcional y se utiliza para asignarle un texto al boton, en caso de no usar este parametro el boton no incluira ningun texto a no ser que se utilice un icono.

```python
self.my_button = Button(self.contenedor_padre, text="custom")
```
Y este es el resultado:

![Button screenshot](./images/simple_button.png)

Por default los botones usan el diseño custom pero puede ser cambiado, mas adelante verás como hacerlo. si quieres cambiar la posición y tamaño de tu boton puedes utilizar el parametro ```pos```

### Pos
Este parametro es global y se utiliza para establecer la posicion de un elemento y para usarlo tienes que enviar un diccionario con los siguientes elementos.

- rileft (Posicion del elemento de izquierda a derecha)
- top	 (Posicion del elemento de arriba a bajo)
- width	 (Largo del elemento)
- height (Altura del elemento)

```Si no se asigna un valor a un elemento del diccionario entonces tomará un valor default```

Puedes asignarle un valor a cada uno de los elementos

```python
self.my_button = Button(self.contenedor_padre, pos={"rileft": 100, "top": 200, "width": 95, "height": 40})
```
como puedes asignarle un valor a elementos especificos

```python
self.my_button = Button(self.contenedor_padre, pos={"rileft": 100, "top": 200})
```
![pos screenshot](./images/button_pos.png)

### Btn
Este parametro aplica uno de los 5 estilos predefinidos al boton 
- primary
- secundary
- success
- danger
- warning

Para aplicar el estilo solo debes enviar un string con el estilo que deseas usar

```python
self.btn1 = Button(self.central_widget, text="Primary", btn='primary')
self.btn2 = Button(self.central_widget, text="Secundary", btn="secundary")
self.btn3 = Button(self.central_widget, text="Success", btn="success")
self.btn4 = Button(self.central_widget, text="Danger", btn="danger")
self.btn5 = Button(self.central_widget, text="Warning", btn="warning")
```
![btn screenshot](./images/button_btn.png)

### Icon
Puedes añadir iconos a tus botones enviando un diccionario con los siguientes elementos.
Iconos gracias a [qtawesome](https://github.com/spyder-ide/qtawesome)

- color (Cambia el color del icono)
- icon 	(Selecciona un icono de las fuentes de iconos disponible)
- icon size (Cambia el tamaño del icono)

##### Fuentes de iconos soportadas
icon identifica iconos a partir de un **prefix** y luego el **nombre del icono**, separado por un (`.`).

Los siguientes prefixes estan disponible para su uso:

- [**FontAwesome**](https://fontawesome.com):

  - FA 5.4.2 contiene 1,393 iconos gratuitos con diferentes estilos.

    - El prefix `fa5` tiene [151 iconos con el estilo "**regular**"](https://fontawesome.com/icons?d=gallery&s=regular&v=5.0.0,5.0.1,5.0.2,5.0.3,5.0.4,5.0.5,5.0.6,5.0.7,5.0.8,5.0.9,5.0.10,5.0.11,5.0.12,5.0.13,5.1.0,5.1.1,5.2.0,5.3.0,5.3.1,5.4.0,5.4.1,5.4.2&m=free)
    - El prefix `fa5s` tiene [852 iconos con el estilo "**solid**"](https://fontawesome.com/icons?d=gallery&s=solid&v=5.0.0,5.0.1,5.0.2,5.0.3,5.0.4,5.0.5,5.0.6,5.0.7,5.0.8,5.0.9,5.0.10,5.0.11,5.0.12,5.0.13,5.1.0,5.1.1,5.2.0,5.3.0,5.3.1,5.4.0,5.4.1,5.4.2&m=free)
    - El prefix `fa5b` tiene [390 iconos con varios **brands**.](https://fontawesome.com/icons?d=gallery&s=brands&v=5.0.0,5.0.1,5.0.2,5.0.3,5.0.4,5.0.5,5.0.6,5.0.7,5.0.8,5.0.9,5.0.10,5.0.11,5.0.12,5.0.13,5.1.0,5.1.1,5.2.0,5.3.0,5.3.1,5.4.0,5.4.1,5.4.2&m=free)



- El prefix `ei` contiene [**Elusive Icons** 2.0 con 304 iconos disponibles.](http://elusiveicons.com/icons/).

- El prefix `mdi` contiene [**Material Design Icons** 3.0.39 con 3039 iconos disponibles.](https://cdn.materialdesignicons.com/3.0.39/)
```python
self.btn1 = Button(self.central_widget, text="fa5 icon", btn='primary', icon={"color": "white", "icon": "fa5.grin-beam", "icon size": 25})
self.btn2 = Button(self.central_widget, text="fa5s icon", btn="secundary", icon={"color": "white", "icon size": 25, "icon": "fa5s.dizzy"})
self.btn3 = Button(self.central_widget, text="fa5b icon", btn="success", icon={"color": "white", "icon size": 25, "icon": "fa5b.css3"})
self.btn4 = Button(self.central_widget, text="ei icon", btn="danger", icon={"color": "white", "icon size": 25, "icon": "ei.bell"})
self.btn5 = Button(self.central_widget, text="mdi icon", btn="warning", icon={"color": "white", "icon size": 25, "icon": "mdi.cart"})
self.btn6 = Button(self.central_widget, icon={"color": "white", "icon size": 25, "icon": "fa5.heart"})
```
![icon screenshot](./images/button_icon.png)

### Stylesheet
`ADVERTENCIA:` usar el parametro stylesheet puede sobreescribir los estilos predefinidos de `btn` y romper el diseño

Stylesheet es un parametro global y se utiliza para asignar estilos css personalizados a los elementos, para usarlo se debe enviar 
un string con los estilos que se desean aplicar al elemento.

`Para una mayor legibilidad del codigo se recomienda crear un script donde se guarden los estilos que se desean aplicar a determinados
elementos.`

```python
from styles.cusotm import estilos

...

self.btn1 = Button(self.central_widget, text="Custom", stylesheet=estilos['boton_morado'])
```
![stylesheet screenshot](./images/button_stylesheet.png)

### Font

## Navbar

## Vertical Navbar

## Textos

## Entrada de texto

## Cards

## Listas

## Notificaciones

## Botones toggle

# ¡Tu primera App!