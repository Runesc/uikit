# !/usr/bin/env python
# coding: utf-8

from modules.UIkit import *

# =============================================
# =           Aplicacion de ejemplo           =
# =============================================
class MainApp(QMainWindow):
    def __init__(self, parent=None):
        super(MainApp, self).__init__(parent=parent)
        self.setWindowTitle("Buttons example")
        self.setMinimumSize(500, 500)

        # -----------  Widget padre  -----------

        self.central_widget = QWidget()
        self.setGeometry(0, 0, self.frameGeometry().width(), self.frameGeometry().height())
        self.setCentralWidget(self.central_widget)
        self.central_widget.setStyleSheet("background:#212121")

        # -----------  Titulo  -----------

        self.label = QLabel("Tipos de botones", self.central_widget)
        widget_padre_width = self.central_widget.frameGeometry().width()
        self.label.setGeometry(0, 10, widget_padre_width, 30)
        self.label.setStyleSheet("color:#fff; font-size:20pt")
        self.label.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

        # -----------  Botones  -----------
        self.btn1 = Button(self.central_widget, text="Primary", pos={"top": 60, "rileft": 10}, btn='primary',
                          icon={"color": "white", "icon": "fa5s.adjust", "icon size": 25})
        self.btn2 = Button(self.central_widget, text="Secundary", btn="secundary", pos={"top": 60, "rileft": 95+10})
        self.btn3 = Button(self.central_widget, text="Success", btn="success", pos={"top": 60, "rileft": 95*2+10})
        self.btn4 = Button(self.central_widget, text="Danger", btn="danger", pos={"top": 60, "rileft": 95*3+10})
        self.btn5 = Button(self.central_widget, text="Warning", btn="warning", pos={"top": 60, "rileft": 95*4+10})
        self.btn6 = Button(self.central_widget, text="Custom", pos={"top": 60, "rileft": 95*5+10})

# ======  End of Aplicacion de ejemplo  =======


if __name__ == "__main__":
    app = QApplication([])
    window = MainApp()
    window.show()
    app.exec_()

