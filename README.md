# UIkit
UIkit es una librería front-end ligera y modular para desarrollar interfaces con PyQt rápidas, con menos código y potentes.

## Requisitos
- Python 3.5+
- PyQt5 5.11.3
- QtAwesome 0.5.3

## Instalación 
Para poder comenzar a usar UIkit en tus proyectos tienes que guardar el archivo ```uikit.py``` en la carpeta contenedora de tu proyecto o en sus subcarpetas y finalmente llamar la libreria en tu proyecto con 

```
from <app path>.UIkit import *
```

